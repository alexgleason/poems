Park
====

Let’s walk through the park and identify trees,  
mull over Elves and unearth golden keys  
to unlock all our secrets and do as we please,  
as we cultivate memories.  
