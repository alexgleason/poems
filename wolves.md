Wolves
======

Woods are welcoming;  
dark, mysterious. In light,  
work days teem with wolves.  
