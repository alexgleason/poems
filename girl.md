Girl
====

Life is hard and I’m so soft  
I’m an earthly girl but with my thoughts aloft  
From the marijuana fumes that oft I’d waft  
And everybody coughed.  
