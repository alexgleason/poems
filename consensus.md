Consensus
=========

Cooperation's complications cause the Khans complaints.  
Could we ever reach consensus or is this thought in vain?  
If vanity creates this struggle, then those who will remain  
take it in stride, set their differences aside, and check both mirrors before changing lanes.
