Cafe
====

I woke up this morning and washed myself clean  
And dressed in a fashion as though I’d be seen.  
A suit and a tie and some mousse for my hair  
The very best slacks in my closet I’d wear.  
The finest red scarf, which my mother had sewn  
and I went to the cafe and ate there alone.  
