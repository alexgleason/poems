Tobacco
=======

Sometimes I wear a red silk Chinese jacket,  
then take some tobacco, and roll it or pack it.  
I rest in the grass, but to you I confide  
that I wish I had a better reason to spend time outside.  
