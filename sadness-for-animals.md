Sadness for Animals
===================

I've got less emotions  
than almost all my peers.  
When they laugh, I only smile,  
and I only frown when they're in tears  

But what we to do animals  
is so inconsiderate and heartless  
at times I cry; they eat half a sandwich  
and then throw away the carcass  

No animal deserves to die;  
they want to live like you and I.  
They've broken no laws yet we slit their paws  
wrap disingenuously with gauze, then break their jaws  
they're different because we promote their flaws  
we hide their pain, bleach the bloody stains  
suppress their voices so they can't have choices  
cramp them in cages 'cause it pays our wages  
slitting their throats when we're shopping and vote  
to contribute to misery instead of to liberty  
so it's time to stand up for them  
and take responsibility  

Stop eating animals  
Their lives depend on it  
