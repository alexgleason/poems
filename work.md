Work
====

I sit at my desk and peer over my shoulder  
for fear of the boss or another scolder.  
He says when I’m fired my body will smolder.  
I must do my work, work until I am older.  
And so I peer over my shoulder.  
