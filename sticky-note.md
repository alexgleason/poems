Sticky Note
===========

I am a lonely sticky note.  
Nobody loves me,  
nobody wants me,  
so on this wall I will stay—  
hoping for love someday.  
