Outside
=======

Why don’t we have lunch outside everyday?  
We used to have recess and play.  
Now that we’re older we’ve grown so much colder  
but surely there’s warmth in the clay.  
