Acrobats
========

Lovely clothes from head to toes  
Watch them strike a dramatic pose.  
Flaming hoop and they jump through it  
Just to prove that they can do it.  
