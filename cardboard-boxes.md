Cardboard Boxes
===============

All of my life is in boxes  
Corrugated cardboard from shoes to stuffed foxes  
Unpacking's not happening they're stacked to the ceiling  
So I guess I'll just live my life stealing  
