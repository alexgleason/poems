Saturday, November 1st
======================

Saturday, November 1st  
They may as well bring out the hearse  

For out my bed I will not rise  
And sleeplessness has shut my eyes  

Each week has worn me down to the bone  
But still I endure ’cause I’ll reap what I’ve sown  

“Enjoy the festivities” is what they have said  
But every Saturday is Day of the Dead  
