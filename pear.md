Pear
====

Again Jack was late to work  
rain splashed and spattered his windshield.  
Jack was the lousiest clerk  
of anyone else in his job field.  

When he finally arrived and sat down in his chair,  
he stared at his screen without even a care.  
So long as he kept his appearance he’d fare,  
he thought as he gobbled a ripe golden pear.  

But under his bite his teeth did reveal  
A sprouting of saplings, a sight so surreal.  
At an instant they grew into a stalk Jack could feel  
would carry him high to a haven ideal.  

He climbed through the clouds toward a glimmering light  
and once at the top saw the most spoken of sights.  
He’d finally reached Heaven, but it was empty and bare.  
All around him just clouds, and nothing else there.  
