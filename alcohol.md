Alcohol
=======

We’re full of spirit  
but I am not more alive  
home’s better—bar none.​  
