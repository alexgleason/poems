A Chocolate Fudge Smoothie
==========================

I tried to lose weight, but O woe is me—  
I couldn’t help drinking that fudge chocolate smoothie.  
Now down the chimneys my body won’t fit.  
The last time I tried the bricks crackled and split.  

And then by the time I had gotten inside  
The folks who were living there were quite terrified.  
I was shot with a rifle and maced in the face  
and dragged to my sled through the blood, snow, and waste.  

I boarded and shouted, “Go Dasher, go Dancer!”  
And to my dismay I did not hear an answer.  
“You’re too large, sir,” said one. No words could have soothed me.  
I should never have drunk that damned chocolate fudge smoothie.  
