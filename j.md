J
===

“Our colors don’t match,”  
but that ain’t true.  
I’m black and white—  
you’re blue.  
