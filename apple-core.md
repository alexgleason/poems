I Eat The Apple Core
====================

When people eat an apple, they only graze the sides,  
Peck left and right and all around, not top nor bottom, nor all inside.  
They don’t dig in deep like I do—it’s tough and tart they say,  
And “but don’t you know? It’s dangerous! The pips contain some cyanide!”  

“Well actually,” is my reply, as I munch upon the core,  
“They contain amygdalin, which metabolizes, sure,  
into that deathly chemical, but I bet you didn’t know,  
the body will detoxify it, so long’s the dose is low.”  

So eat it all, why would you not?  
Don’t leave that core out just to rot.  
And think before you toss it, that starving kids would love to have it.  
They’d even eat over all the spots where you already bit into it.  

Without even cutting that part off.  
