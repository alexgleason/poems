I've always liked comparing work to romantic relationships. So this time I decided to try it in the other direction. This is a full chronology of my romantic pursuits, expressed as though they were job employments.

 ------------

I worked at a library. We seldom spoke, and only in whispers, so I was never certain what my job was. After some time of negligence I was quietly informed that my contract had been terminated. (2 years)

I took a temporary job I knew nothing about. They had once casually expressed interest in hiring me. I played it off too hard and embarrassed myself when I couldn't do the work. I hope the manager forgives me. (1 week)

I worked for a humble art and magic store. I had a lot of fun, but I felt like I could accomplish more. After leaving, I've sometimes questioned if a more ambitious and complicated life is truly better. (2 years)

I worked for an engineering firm. The management was primarily quirky, older men, and there was a sense of seriousness and self-imposed bureaucracy about it. But it was somewhat balanced out by a curious appreciation of art and music. I learned more about life and work here than anywhere else, and the work was quite mentally stimulating. Though eventually I was fired after an outburst. It was my fault, but I felt like management was holding unrealistic expectations of me. 6 months later I tried applying again, but they selected a different candidate instead. I don't blame them. (2 years)

I worked for a start-up. This small agile team was incredibly artistic, technically-capable, and progressive. This short-lived experience was a vibrant and inspirational journey in which I was challenged on my preconceptions and encouraged to direct my energy towards shaping a better world. We knew that things don't have to be the way they are, and our company could be the ones to change them. But in the end, they were moving too fast, and I couldn't keep up. (1 year)

I'm unemployed, everyone. (present)
