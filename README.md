Poems
========
This is a repo of open source poems. They’re all licensed under [CC-BY-SA](http://creativecommons.org/licenses/by-sa/4.0/). Feel free to improve them with a PR or add your own.
